import mods.advancedmortars.Mortar;

# plant fiber
Mortar.addRecipe(
  ["wood", "stone", "iron"],
  <primal:plant_fiber_pulp>,
  8,
  [<primal:plant_tinder>]);

# resin
Mortar.addRecipe(
  ["wood", "stone", "iron"],
  <primal:tannin_ground>,
  8,
  [<ore:barkWood>]);
