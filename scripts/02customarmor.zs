#loader contenttweaker

import mods.contenttweaker.MaterialSystem;

var pelt = MaterialSystem.getMaterialBuilder()
  .setName("Pelt")
  .setColor(10251082)
  .build();

var pelt_armor = pelt.registerPart("armor");
pelt_armor.setColorized(false);

var pelt_armor_data = pelt_armor.getData();
pelt_armor_data.addDataValue("durability", "4");
pelt_armor_data.addDataValue("reduction", "1,1,1,1");
