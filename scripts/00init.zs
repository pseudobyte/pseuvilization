var nuclear = false;

# remove all standard recipes in nuclear mode
if (nuclear) {
  recipes.removeAll();
} 
# otherwise remove conflicting recipes
else {
  # primal crafting table recipes; conflict with basic worktable
  recipes.removeByRegex("primal:worktable_shelf(?:_[1-9])?");

  # primal early game recipes
  recipes.removeByRecipeName("primal:cordage_plant");
  recipes.removeByRecipeName("primal:flint_hatchet");
  recipes.removeByRecipeName("primal:bone_hatchet");
  recipes.removeByRecipeName("primal:yew_seed");
  recipes.removeByRecipeName("primal:lit_torch_nether");
}
