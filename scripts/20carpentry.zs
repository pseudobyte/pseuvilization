import mods.artisanworktables.builder.RecipeBuilder;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import crafttweaker.item.IItemStack;

function recipe_id(suffix as string) as string {
  return "pseu_carpentry_" + suffix;
}

var splitLog = <ore:logSplit>;
var stickWood = <ore:stickWood>;
var plankAny = <ore:plankWood>;
var pinAny = <ore:pinBasic>;
var cordage = <ore:cordageGeneral>;
var resin = <primal:tannin_ground>;

# general ingredients
RecipeBuilder.get("carpenter")
  .setName(recipe_id("stick"))
  .setShapeless([splitLog])
  .addTool(<ore:artisansHandsaw>, 1)
  .addOutput(<minecraft:stick> * 4)
  .create();
RecipeBuilder.get("carpenter")
  .setName(recipe_id("wood_pin"))
  .setShapeless([stickWood])
  .addTool(<ore:artisansHandsaw>, 1)
  .addOutput(<primal:wood_pin> * 2)
  .create();

# workstations
RecipeBuilder.get("carpenter")
  .setName(recipe_id("basic_worktable"))
  .setShaped([
    [plankAny, plankAny],
    [plankAny, plankAny]])
  .addTool(<ore:artisansFramingHammer>, 1)
  .addOutput(<artisanworktables:worktable:5>)
  .create();
RecipeBuilder.get("basic")
  .setName(recipe_id("carpenter_worktable_basic"))
  .setShaped([[null, <ore:logSplit>, null], [<ore:logSplit>, <artisanworktables:worktable:5>, <ore:logSplit>], [null, <ore:logSplit>, null]])
  .addOutput(<artisanworktables:worktable:1>)
  .create();
RecipeBuilder.get("carpenter")
  .setName(recipe_id("carpenter_worktable_carpenter"))
  .setShaped([[null, <ore:logSplit>, null], [<ore:logSplit>, <artisanworktables:worktable:5>, <ore:logSplit>], [null, <ore:logSplit>, null]])
  .addTool(<ore:artisansFramingHammer>, 1)
  .addOutput(<artisanworktables:worktable:1>)
  .create();
RecipeBuilder.get("carpenter")
  .setName(recipe_id("scribe_worktable"))
  .setShaped([[null, <ore:paper>, null], [<ore:paper>, <artisanworktables:worktable:5>, <ore:paper>], [null, <ore:paper>, null]])
  .addTool(<ore:artisansFramingHammer>, 1)
  .addOutput(<artisanworktables:worktable:8>)
  .create();
RecipeBuilder.get("carpenter")
  .setName(recipe_id("farmer_worktable"))
  .setShaped([[<primal:mud_clump>, <primal:mud_clump>, <primal:mud_clump>], [null, <artisanworktables:worktable:5>, null]])
  .addTool(<ore:artisansFramingHammer>, 1)
  .addOutput(<artisanworktables:worktable:10>)
  .create();
RecipeBuilder.get("carpenter")
  .setName(recipe_id("wood_mortar"))
  .setShaped([[null, null, stickWood], [plankAny, <minecraft:flint>, plankAny], [null, plankAny, null]])
  .addTool(<ore:artisansFramingHammer>, 1)
  .addOutput(<advancedmortars:mortar>)
  .create();

# any-wood recipes
RecipeBuilder.get("carpenter")
  .setName(recipe_id("sign"))
  .setShaped([
    [<ore:thinWood>],
    [stickWood]])
  .addTool(<ore:artisansFramingHammer>, 1)
  .addOutput(<minecraft:sign>)
  .create();

# per-wood recipes
var woodTypes = ["oak", "spruce", "birch", "jungle", "acacia", "darkoak", "ironwood", "yew", "corphyry"] as string[];
var woodClass = ["vanilla", "vanilla", "vanilla", "vanilla", "vanilla", "vanilla", "primal", "primal", "primal"] as string[];
var splitWood = [<ore:splitOak>, <ore:splitSpruce>, <ore:splitBirch>, <ore:splitJungle>, <ore:splitAcacia>, <ore:splitDarkOak>, <ore:splitIronwood>, <ore:splitYew>, <ore:splitCorypha>] as IIngredient[];
var strippedWood = [<ore:strippedOak>, <ore:strippedSpruce>, <ore:strippedBirch>, <ore:strippedJungle>, <ore:strippedAcacia>, <ore:strippedDarkOak>, <ore:strippedIronwood>, <ore:strippedYew>, <ore:strippedCorypha>] as IIngredient[];
var plankInput = [<ore:plankOak>, <ore:plankSpruce>, <ore:plankBirch>, <ore:plankJungle>, <ore:plankAcacia>, <ore:plankDarkOak>, <ore:plankIronwood>, <ore:plankYew>, <ore:plankCorypha>] as IOreDictEntry[];
var plankOutput = [<minecraft:planks>, <minecraft:planks:1>, <minecraft:planks:2>, <minecraft:planks:3>, <minecraft:planks:4>, <minecraft:planks:5>, <primal:planks>, <primal:planks:1>, <primal:planks:3>] as IItemStack[];
var vertPlank = [<quark:vertical_planks>, <quark:vertical_planks:1>, <quark:vertical_planks:2>, <quark:vertical_planks:3>, <quark:vertical_planks:4>, <quark:vertical_planks:5>, null, null, null] as IItemStack[];
var carvedWood = [<quark:carved_wood>, <quark:carved_wood:1>, <quark:carved_wood:2>, <quark:carved_wood:3>, <quark:carved_wood:4>, <quark:carved_wood:5>, null, null, null] as IItemStack[];
var slabs = [<minecraft:wooden_slab>, <minecraft:wooden_slab:1>, <minecraft:wooden_slab:2>, <minecraft:wooden_slab:3>, <minecraft:wooden_slab:4>, <minecraft:wooden_slab:5>, <primal:slab_ironwood>, <primal:slab_yew>, <primal:slab_corypha>] as IItemStack[];
var thin_slabs = [<primal:thin_slab_oak>, <primal:thin_slab_spruce>, <primal:thin_slab_birch>, <primal:thin_slab_jungle>, <primal:thin_slab_acacia>, <primal:thin_slab_bigoak>, <primal:thin_slab_ironwood>, <primal:thin_slab_yew>, <primal:thin_slab_corypha>] as IItemStack[];
var slats = [<primal:slat_oak>, <primal:slat_spruce>, <primal:slat_birch>, <primal:slat_jungle>, <primal:slat_acacia>, <primal:slat_bigoak>, <primal:slat_ironwood>, <primal:slat_yew>, <primal:slat_corypha>] as IItemStack[];
var stairs = [<minecraft:oak_stairs>, <minecraft:spruce_stairs>, <minecraft:birch_stairs>, <minecraft:jungle_stairs>, <minecraft:acacia_stairs>, <minecraft:dark_oak_stairs>, <primal:stairs_ironwood>, null, <primal:stairs_corypha>] as IItemStack[];
var chests = [<minecraft:chest>, <quark:custom_chest>, <quark:custom_chest:1>, <quark:custom_chest:2>, <quark:custom_chest:3>, <quark:custom_chest:4>, null, null, null] as IItemStack[];
var fences = [<minecraft:fence>, <minecraft:spruce_fence>, <minecraft:birch_fence>, <minecraft:jungle_fence>, <minecraft:acacia_fence>, <minecraft:dark_oak_fence>, <primal:fence>, <primal:fence:1>, <primal:fence:3>] as IItemStack[];
var gates = [<minecraft:fence_gate>, <minecraft:spruce_fence_gate>, <minecraft:birch_fence_gate>, <minecraft:jungle_fence_gate>, <minecraft:acacia_fence_gate>, <minecraft:dark_oak_fence_gate>, <primal:gate_ironwood>, <primal:gate_yew>, <primal:gate_corypha>] as IItemStack[];
var doors = [<minecraft:wooden_door>, <minecraft:spruce_door>, <minecraft:birch_door>, <minecraft:jungle_door>, <minecraft:acacia_door>, <minecraft:dark_oak_door>, <primal:door_ironwood>, <primal:door_yew>, <primal:door_corypha>] as IItemStack[];
var trapdoors = [<minecraft:trapdoor>, <quark:spruce_trapdoor>, <quark:birch_trapdoor>, <quark:jungle_trapdoor>, <quark:acacia_trapdoor>, <quark:dark_oak_trapdoor>, null, null, null] as IItemStack[];
var boats = [<minecraft:boat>, <minecraft:spruce_boat>, <minecraft:birch_boat>, <minecraft:jungle_boat>, <minecraft:acacia_boat>, <minecraft:dark_oak_boat>, <primal:boat_ironwood>, <primal:boat_yew>, <primal:boat_corypha>] as IItemStack[];
var barrels = [<primal:barrel>, <primal:barrel:1>, <primal:barrel:2>, <primal:barrel:3>, <primal:barrel:4>, <primal:barrel:5>, <primal:barrel:6>, <primal:barrel:7>, <primal:barrel:9>] as IItemStack[];
var barrel_lids = [<primal:barrel_oak_lid>, <primal:barrel_spruce_lid>, <primal:barrel_birch_lid>, <primal:barrel_jungle_lid>, <primal:barrel_acacia_lid>, <primal:barrel_dark_oak_lid>, <primal:barrel_ironwood_lid>, <primal:barrel_yew_lid>, <primal:barrel_corypha_lid>] as IItemStack[];
var drying_racks = [<primal:drying_rack>.withTag({type: "oak"}), <primal:drying_rack:1>.withTag({type: "spruce"}), <primal:drying_rack:2>.withTag({type: "birch"}), <primal:drying_rack:3>.withTag({type: "jungle"}), <primal:drying_rack:4>.withTag({type: "acacia"}), <primal:drying_rack:5>.withTag({type: "dark_oak"}), <primal:drying_rack:6>.withTag({type: "ironwood"}), <primal:drying_rack:7>.withTag({type: "yew"}), <primal:drying_rack:9>.withTag({type: "corypha"})] as IItemStack[];
var fish_traps = [<primal:fish_trap>.withTag({type: "oak"}), <primal:fish_trap:1>.withTag({type: "spruce"}), <primal:fish_trap:2>.withTag({type: "birch"}), <primal:fish_trap:3>.withTag({type: "jungle"}), <primal:fish_trap:4>.withTag({type: "acacia"}), <primal:fish_trap:5>.withTag({type: "dark_oak"}), <primal:fish_trap:6>.withTag({type: "ironwood"}), <primal:fish_trap:7>.withTag({type: "yew"}), <primal:fish_trap:9>.withTag({type: "corypha"})] as IItemStack[];
var brick_molds = [<primal:brick_mold>.withTag({type: "oak"}), <primal:brick_mold:1>.withTag({type: "spruce"}), <primal:brick_mold:2>.withTag({type: "birch"}), <primal:brick_mold:3>.withTag({type: "jungle"}), <primal:brick_mold:4>.withTag({type: "acacia"}), <primal:brick_mold:5>.withTag({type: "dark_oak"}), <primal:brick_mold:6>.withTag({type: "ironwood"}), <primal:brick_mold:7>.withTag({type: "yew"}), <primal:brick_mold:9>.withTag({type: "corypha"})] as IItemStack[];
var thatch_block = <quark:thatch>;
var tatami_beds = [<primal:tatami_bed>.withTag({type: "oak"}), <primal:tatami_bed:1>.withTag({type: "spruce"}), <primal:tatami_bed:2>.withTag({type: "birch"}), <primal:tatami_bed:3>.withTag({type: "jungle"}), <primal:tatami_bed:4>.withTag({type: "acacia"}), <primal:tatami_bed:5>.withTag({type: "dark_oak"}), <primal:tatami_bed:6>.withTag({type: "ironwood"}), <primal:tatami_bed:7>.withTag({type: "yew"}), <primal:tatami_bed:9>.withTag({type: "corypha"})] as IItemStack[];
var ladders = [<primal:ladder_oak>, <primal:ladder_spruce>, <primal:ladder_birch>, <primal:ladder_jungle>, <primal:ladder_acacia>, <primal:ladder_bigoak>, <primal:ladder_ironwood>, <primal:ladder_yew>, <primal:ladder_corypha>] as IItemStack[];
var drawer1 = [<storagedrawers:basicdrawers>.withTag({material: "oak"}), <storagedrawers:basicdrawers>.withTag({material: "spruce"}), <storagedrawers:basicdrawers>.withTag({material: "birch"}), <storagedrawers:basicdrawers>.withTag({material: "jungle"}), <storagedrawers:basicdrawers>.withTag({material: "acacia"}), <storagedrawers:basicdrawers>.withTag({material: "dark_oak"}), null, null, null] as IItemStack[];
var drawer2 = [<storagedrawers:basicdrawers:1>.withTag({material: "oak"}), <storagedrawers:basicdrawers:1>.withTag({material: "spruce"}), <storagedrawers:basicdrawers:1>.withTag({material: "birch"}), <storagedrawers:basicdrawers:1>.withTag({material: "jungle"}), <storagedrawers:basicdrawers:1>.withTag({material: "acacia"}), <storagedrawers:basicdrawers:1>.withTag({material: "dark_oak"}), null, null, null] as IItemStack[];
var drawer4 = [<storagedrawers:basicdrawers:2>.withTag({material: "oak"}), <storagedrawers:basicdrawers:2>.withTag({material: "spruce"}), <storagedrawers:basicdrawers:2>.withTag({material: "birch"}), <storagedrawers:basicdrawers:2>.withTag({material: "jungle"}), <storagedrawers:basicdrawers:2>.withTag({material: "acacia"}), <storagedrawers:basicdrawers:2>.withTag({material: "dark_oak"}), null, null, null] as IItemStack[];
var drawer2h = [<storagedrawers:basicdrawers:3>.withTag({material: "oak"}), <storagedrawers:basicdrawers:3>.withTag({material: "spruce"}), <storagedrawers:basicdrawers:3>.withTag({material: "birch"}), <storagedrawers:basicdrawers:3>.withTag({material: "jungle"}), <storagedrawers:basicdrawers:3>.withTag({material: "acacia"}), <storagedrawers:basicdrawers:3>.withTag({material: "dark_oak"}), null, null, null] as IItemStack[];
var drawer4h = [<storagedrawers:basicdrawers:4>.withTag({material: "oak"}), <storagedrawers:basicdrawers:4>.withTag({material: "spruce"}), <storagedrawers:basicdrawers:4>.withTag({material: "birch"}), <storagedrawers:basicdrawers:4>.withTag({material: "jungle"}), <storagedrawers:basicdrawers:4>.withTag({material: "acacia"}), <storagedrawers:basicdrawers:4>.withTag({material: "dark_oak"}), null, null, null] as IItemStack[];
var drawertrim = [<storagedrawers:trim>, <storagedrawers:trim:1>, <storagedrawers:trim:2>, <storagedrawers:trim:3>, <storagedrawers:trim:4>, <storagedrawers:trim:5>, null, null, null] as IItemStack[];

for i, wood in woodTypes {
  # split wood to planks
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_planks"))
    .setShaped([
      [splitWood[i], splitWood[i]],
      [splitWood[i], splitWood[i]]])
    .addTool(<ore:artisansHandsaw>, 1)
    .addOutput(plankOutput[i] * 4)
    .create();
  # split wood to slabs
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_slab_split"))
    .setShaped([
      [splitWood[i], splitWood[i], splitWood[i]]])
    .addTool(<ore:artisansHandsaw>, 1)
    .addOutput(slabs[i] * 6)
    .create();
  # planks to slabs
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_slab_plank"))
    .setShapeless([plankInput[i]])
    .addTool(<ore:artisansHandsaw>, 1)
    .addOutput(slabs[i] * 2)
    .create();
  # split wood to thin slabs
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_thinslab_split"))
    .setShaped([
      [splitWood[i], splitWood[i], splitWood[i]]])
    .addTool(<ore:artisansHandsaw>, 1)
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(thin_slabs[i] * 12)
    .create();
  # slabs to thin slabs
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_thinslab_slab"))
    .setShapeless([slabs[i]])
    .addTool(<ore:artisansHandsaw>, 1)
    .addOutput(thin_slabs[i] * 2)
    .create();
  # split wood to slats
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_slat_split"))
    .setShaped([
      [splitWood[i], null, splitWood[i]],
      [splitWood[i], null, splitWood[i]]])
    .addTool(<ore:artisansHandsaw>, 1)
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(slats[i] * 32)
    .create();
  # thin slabs to slats
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_slat_thinslab"))
    .setShapeless([thin_slabs[i]])
    .addTool(<ore:artisansHandsaw>, 1)
    .addOutput(slats[i] * 2)
    .create();
  # planks to stairs
  if (woodTypes[i] != "yew") {
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_stairs"))
      .setShaped([
        [plankInput[i], null, null],
        [plankInput[i], plankInput[i], null],
        [plankInput[i], plankInput[i], plankInput[i]]])
      .addTool(<ore:artisansHandsaw>, 1)
      .addOutput(stairs[i] * 8)
      .create();
  }
  # fence
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_fence"))
    .setShaped([
      [slats[i], pinAny, slats[i]],
      [slats[i], pinAny, slats[i]]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(fences[i] * 3)
    .create();
  # gate
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_gate"))
    .setShaped([
      [slats[i], thin_slabs[i], slats[i]],
      [pinAny, thin_slabs[i], pinAny]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(gates[i])
    .create();
  # door
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_door"))
    .setShaped([
      [thin_slabs[i], thin_slabs[i]],
      [thin_slabs[i], thin_slabs[i]],
      [thin_slabs[i], thin_slabs[i]]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(doors[i])
    .create();
  # boat
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_boat"))
    .setShaped([
      [plankInput[i], null, plankInput[i]],
      [resin, plankInput[i], resin]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(boats[i])
    .create();
  # barrel
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_barrel"))
    .setShaped([
      [slabs[i], null, slabs[i]],
      [slabs[i], null, slabs[i]],
      [resin, slabs[i], resin]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(barrels[i])
    .create();
  # barrel lid
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_barrellid"))
    .setShapeless([thin_slabs[i], resin])
    .addTool(<ore:artisansHandsaw>, 1)
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(barrel_lids[i])
    .create();
  # drying rack
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_dryingrack"))
    .setShaped([
      [slats[i], slats[i], slats[i]],
      [pinAny, null, pinAny],
      [plankInput[i], null, plankInput[i]]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(drying_racks[i])
    .create();
  # fish trap
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_fishtrap"))
    .setShaped([
      [cordage, slats[i], cordage],
      [slats[i], null, slats[i]],
      [cordage, slats[i], cordage]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(fish_traps[i])
    .create();
  # brick mold
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_brickmold"))
    .setShaped([
      [splitWood[i], null, splitWood[i]],
      [null, splitWood[i], null]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(brick_molds[i])
    .create();
  # tatami bed
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_tatamibed"))
    .setShaped([
      [thatch_block, thatch_block, thatch_block],
      [plankInput[i], plankInput[i], plankInput[i]]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(tatami_beds[i])
    .create();
  # ladder
  RecipeBuilder.get("carpenter")
    .setName(recipe_id(wood + "_ladder"))
    .setShaped([
      [slats[i], null, slats[i]],
      [pinAny, slats[i], pinAny],
      [slats[i], null, slats[i]]])
    .addTool(<ore:artisansFramingHammer>, 1)
    .addOutput(ladders[i])
    .create();
  if (woodClass[i] == "vanilla") {
    # vertical planks
    plankInput[i].add(vertPlank[i]);
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_vertPlank"))
      .setShaped([
        [plankInput[i]],
        [plankInput[i]],
        [plankInput[i]]])
      .addOutput(vertPlank[i] * 3)
      .create();
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_horizPlank"))
      .setShaped([[plankInput[i], plankInput[i], plankInput[i]]])
      .addOutput(plankOutput[i] * 3)
      .create();
    # carved wood
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_carvedWood"))
      .setShapeless([strippedWood[i]])
      .addOutput(carvedWood[i] * 4)
      .create();    
    # chest
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_chest"))
      .setShaped([
        [plankInput[i], plankInput[i], plankInput[i]],
        [plankInput[i], null, plankInput[i]],
        [plankInput[i], plankInput[i], plankInput[i]]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(chests[i])
      .create();
    # trapdoor
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_trapdoor"))
      .setShaped([
        [thin_slabs[i], thin_slabs[i], thin_slabs[i]]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(trapdoors[i])
      .create();
    # 1x1 drawer
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_drawer1"))
      .setShaped([
        [null, slabs[i], null],
        [slabs[i], <ore:chestWood>, slabs[i]],
        [null, slabs[i], null]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(drawer1[i])
      .create();
    # 1x2 drawer
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_drawer2"))
      .setShaped([
        [slabs[i], <ore:chestWood>, slabs[i]],
        [slabs[i], slabs[i], slabs[i]],
        [slabs[i], <ore:chestWood>, slabs[i]]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(drawer2[i])
      .create();
    # 1x2 thin drawer
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_drawer2h"))
      .setShaped([
        [thin_slabs[i], <ore:chestWood>, thin_slabs[i]],
        [thin_slabs[i], thin_slabs[i], thin_slabs[i]],
        [thin_slabs[i], <ore:chestWood>, thin_slabs[i]]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(drawer2h[i])
      .create();
    # 2x2 drawer
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_drawer4"))
      .setShaped([
        [<ore:chestWood>, slabs[i], <ore:chestWood>],
        [slabs[i], slabs[i], slabs[i]],
        [<ore:chestWood>, slabs[i], <ore:chestWood>]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(drawer4[i])
      .create();
    # 2x2 thin drawer
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_drawer4h"))
      .setShaped([
        [<ore:chestWood>, thin_slabs[i], <ore:chestWood>],
        [thin_slabs[i], thin_slabs[i], thin_slabs[i]],
        [<ore:chestWood>, thin_slabs[i], <ore:chestWood>]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(drawer4h[i])
      .create();
    # drawer trim
    RecipeBuilder.get("carpenter")
      .setName(recipe_id(wood + "_drawertrim"))
      .setShaped([
        [stickWood, thin_slabs[i], stickWood],
        [thin_slabs[i], plankInput[i], thin_slabs[i]],
        [stickWood, thin_slabs[i], stickWood]])
      .addTool(<ore:artisansFramingHammer>, 1)
      .addOutput(drawertrim[i])
      .create();
  }
}
