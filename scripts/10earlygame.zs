import mods.artisanworktables.builder.RecipeBuilder;
import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import mods.inworldcrafting.FireCrafting;

var crafting_torch_light = false;

function recipe_id(suffix as string) as string {
  return "pseu_earlygame_" + suffix;
}

# add flint and rocks to grass
vanilla.seeds.addSeed(<minecraft:flint> % 4);
vanilla.seeds.addSeed(<primal:rock_stone> % 2);

# switch seed drops to grass
vanilla.seeds.removeSeed(<immersiveengineering:seed>);
vanilla.seeds.removeSeed(<minecraft:wheat_seeds>);
vanilla.seeds.addSeed(<primal:tall_grass_seeds> % 10);

# plant fiber cordage
recipes.addShapeless(recipe_id("cordage_plant"), <primal:plant_cordage>, [<ore:fiberPlant>, <ore:fiberPlant>, <ore:fiberPlant>]);
# basic worktable
recipes.addShaped(recipe_id("basic_worktable"), <artisanworktables:worktable:5>, [[<ore:toolAxe>, <ore:logWoodWhole>]]);
# remove yew seed
recipes.addShapeless(recipe_id("yew_seed"), <primal:yew_seed>, [<primal:yew_aril>, <ore:toolWorkBlade>]);

var toolTypes = ["flint", "bone"] as string[];
var flakes = [<ore:flakeFlint>, <ore:flakeBone>] as IIngredient[];
var points = [<ore:pointFlint>, <ore:pointBone>] as IIngredient[];
var cordage = <ore:cordageGeneral>;
var stick = <ore:stickWood>;
var hatchets = [<primal:flint_hatchet>, <primal:bone_hatchet>] as IItemStack[];
var pickaxes = [<primal:flint_pickaxe>, <primal:bone_pickaxe>] as IItemStack[];
var primitive_pickaxes = false;
var shovels = [<primal:flint_shovel>, <primal:bone_shovel>] as IItemStack[];
var hoes = [<primal:flint_hoe>, <primal:bone_hoe>] as IItemStack[];
var shears = [<primal:flint_shears>, <primal:bone_shears>] as IItemStack[];
var primitive_shears = false;
var swords = [<primal:sword_crude_flint>, <primal:sword_crude_bone>] as IItemStack[];
var axes = [<primal:flint_axe>] as IItemStack[];
var workblades = [<primal:flint_workblade>] as IItemStack[];
var handsaws = [<artisanworktables:artisans_handsaw_flint>, <artisanworktables:artisans_handsaw_bone>] as IItemStack[];
var framing_hammers = [<artisanworktables:artisans_framing_hammer_flint>, <artisanworktables:artisans_framing_hammer_bone>] as IItemStack[];

# basic tools
for i, mat in toolTypes {
  # hatchet by hand
  recipes.addShaped(recipe_id(mat + "_hatchet"), hatchets[i], [[flakes[i], cordage], [null, stick]]);
  # hatchet by table
  RecipeBuilder.get("basic")
    .setName(recipe_id(mat + "_hatchet_wt"))
    .setShaped(
      [[flakes[i], cordage],
      [null, stick]])
    .addOutput(hatchets[i])
    .create();
  # pickaxes
  if (primitive_pickaxes) {
    RecipeBuilder.get("basic")
      .setName(recipe_id(mat + "_pickaxe"))
      .setShaped(
        [[flakes[i], cordage, flakes[i]],
        [flakes[i], stick, flakes[i]],
        [null, stick, null]])
      .addOutput(pickaxes[i])
      .create();
  }
  # shovels
  RecipeBuilder.get("basic")
    .setName(recipe_id(mat + "_shovel"))
    .setShaped(
      [[null, flakes[i], flakes[i]],
      [null, cordage, flakes[i]],
      [stick, null, null]])
    .addOutput(shovels[i])
    .create();
  # hoes
  RecipeBuilder.get("basic")
    .setName(recipe_id(mat + "_hoe"))
    .setShaped(
      [[flakes[i], cordage],
      [stick, null]])
    .addOutput(hoes[i])
    .create();
  # shears
  if (primitive_shears) {
    RecipeBuilder.get("basic")
      .setName(recipe_id(mat + "_shears"))
      .setShaped(
        [[null, points[i]],
        [points[i], cordage]])
      .addOutput(shears[i])
      .create();
  }
  # crude swords
  RecipeBuilder.get("basic")
    .setName(recipe_id(mat + "_sword_crude"))
    .setShaped(
      [[null, points[i], flakes[i]],
      [points[i], flakes[i], null],
      [stick, cordage, null]])
    .addOutput(swords[i])
    .create();
  if (mat == "flint") {
    # flint axe
    RecipeBuilder.get("basic")
      .setName(recipe_id(mat + "_axe"))
      .setShaped(
        [[flakes[i], cordage, flakes[i]],
        [flakes[i], stick, null],
        [null, stick, null]])
      .addOutput(axes[i])
      .create();
    # flint workblade
    RecipeBuilder.get("basic")
      .setName(recipe_id(mat + "_workblade"))
      .setShaped(
        [[null, flakes[i], points[i]],
        [flakes[i], cordage, flakes[i]],
        [stick, flakes[i], null]])
      .addOutput(workblades[i])
      .create();
    # flint handsaw
    RecipeBuilder.get("basic")
      .setName(recipe_id(mat + "_artisans_handsaw"))
      .setShaped(
        [[flakes[i], cordage],
        [flakes[i], stick]])
      .addOutput(handsaws[i])
      .create();
  }
  if (mat == "bone") {
    # bone framing hammer
    RecipeBuilder.get("basic")
      .setName(recipe_id(mat + "_artisans_framing_hammer"))
      .setShaped(
        [[flakes[i], flakes[i]],
        [flakes[i], cordage],
        [stick, null]])
      .addOutput(framing_hammers[i])
      .create();    
  }
}

# worktable versions of basic recipes
RecipeBuilder.get("basic")
  .setName(recipe_id("cordage_plant_wt"))
  .setShapeless([<ore:fiberPlant>, <ore:fiberPlant>, <ore:fiberPlant>])
  .addOutput(<primal:plant_cordage>)
  .create();
RecipeBuilder.get("basic")
  .setName(recipe_id("yew_seed_wt"))
  .setShapeless([<primal:yew_aril>])
  .addTool(<ore:toolWorkBlade>, 1)
  .addOutput(<primal:yew_seed>)
  .setExtraOutputOne(<primal:yew_aril_seedless>, 1.0)
  .create();
RecipeBuilder.get("basic")
  .setName(recipe_id("basic_worktable_wt"))
  .setShapeless([<ore:logWoodWhole>])
  .addTool(<ore:toolAxe>, 1)
  .addOutput(<artisanworktables:worktable:5>)
  .create();

# campfire
RecipeBuilder.get("basic")
  .setName(recipe_id("campfire"))
  .setShaped(
    [[null, <ore:logCampfire>, null],
    [<ore:logCampfire>, <ore:logCampfire>, <ore:logCampfire>],
    [<ore:rock>, <ore:tinder>, <ore:rock>]])
  .addOutput(<toughasnails:campfire>)
  .create();
RecipeBuilder.get("basic")
  .setName(recipe_id("plant_tinder"))
  .setShaped(
    [[<primal:plant_fiber>, <primal:plant_fiber>],
    [<primal:plant_fiber>, <primal:plant_fiber>]])
  .addOutput(<primal:plant_tinder>)
  .create();

# emergency soil processing
RecipeBuilder.get("basic")
  .setName(recipe_id("gravel_processing"))
  .setShaped(
    [[<ore:gravel>, <ore:gravel>, <ore:gravel>],
    [<ore:gravel>, <ore:gravel>, <ore:gravel>],
    [<ore:gravel>, <ore:gravel>, <ore:gravel>]])
  .addTool(<ore:toolShovel>, 10)
  .addOutput(<minecraft:flint> * 4, 25)
  .addOutput(<minecraft:flint> * 5, 50)
  .addOutput(<minecraft:flint> * 6, 25)
  .setExtraOutputOne(<primal:rock_stone>, 0.25)
  .create();
RecipeBuilder.get("basic")
  .setName(recipe_id("dirt_processing"))
  .setShaped(
    [[<ore:dirt>, <ore:dirt>, <ore:dirt>],
    [<ore:dirt>, <ore:dirt>, <ore:dirt>],
    [<ore:dirt>, <ore:dirt>, <ore:dirt>]])
  .addTool(<ore:toolShovel>, 10)
  .addOutput(<primal:plant_fiber> * 12, 10)
  .addOutput(<primal:plant_fiber> * 13, 15)
  .addOutput(<primal:plant_fiber> * 14, 30)
  .addOutput(<primal:plant_fiber> * 15, 15)
  .addOutput(<primal:plant_fiber> * 16, 10)
  .setExtraOutputOne(<primal:rock_stone>, 0.2)
  .setExtraOutputTwo(<primal:rock_stone>, 0.1)
  .setExtraOutputThree(<minecraft:flint>, 0.1)
  .create();

# charcoal
RecipeBuilder.get("basic")
  .setName(recipe_id("log_pile"))
  .setShaped(
    [[<ore:logCampfire>, <ore:logCampfire>, <ore:logCampfire>],
    [<ore:logCampfire>, <ore:logCampfire>, <ore:logCampfire>],
    [<ore:logCampfire>, <ore:logCampfire>, <ore:logCampfire>]])
  .addOutput(<charcoal_pit:log_pile>)
  .create();
RecipeBuilder.get("basic")
  .setName(recipe_id("fire_starter"))
  .setShaped(
    [[stick, null],
    [<primal:plant_fiber>, stick]])
  .addOutput(<charcoal_pit:fire_starter>)
  .create();

# torches
RecipeBuilder.get("basic")
  .setName(recipe_id("fire_bow"))
  .setShaped(
    [[stick, null],
    [cordage, stick]])
  .addOutput(<primal:fire_bow>)
  .create();
RecipeBuilder.get("basic")
  .setName(recipe_id("torch"))
  .setShaped(
    [[<minecraft:coal:1>],
    [cordage],
    [stick]])
  .addOutput(<primal:torch_wood> * 6)
  .create();
if (crafting_torch_light) {
  RecipeBuilder.get("basic")
    .setName(recipe_id("light_torch_wt"))
    .setShapeless(
      [<primal:torch_wood>, <minecraft:torch>.reuse()])
    .addOutput(<minecraft:torch>)
    .create();
  recipes.addShapeless(recipe_id("light_torch_hand"), <minecraft:torch>, [<primal:torch_wood>, <minecraft:torch>.reuse()]);
}
# guide
var survival_guide = <patchouli:guide_book>.withTag({"patchouli:book": "patchouli:survival"});
var fire_guide = <patchouli:guide_book>.withTag({"patchouli:book": "patchouli:firestarting"});
RecipeBuilder.get("basic")
  .setName(recipe_id("survival_guide"))
  .setShaped(
    [[cordage, <minecraft:flint>, cordage],
    [cordage, stick, cordage],
    [cordage, <minecraft:flint>, cordage]])
  .addOutput(survival_guide)
  .create();
FireCrafting.addRecipe(fire_guide, survival_guide);

# pottery
RecipeBuilder.get("basic")
  .setName(recipe_id("mason_worktable"))
  .setShaped([[null, <ore:brickDry>, null], [<ore:brickDry>, <artisanworktables:worktable:5>, <ore:brickDry>], [null, <ore:brickDry>, null]])
  .addOutput(<artisanworktables:worktable:14>)
  .create();
