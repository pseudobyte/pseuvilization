# whole, unstripped logs
var wood_raw = <ore:logWoodRaw>;

wood_raw.add(<minecraft:log:*>);
wood_raw.add(<minecraft:log2:*>);
wood_raw.add(<primal:logs:*>);
wood_raw.add(<primal:corypha_stalk>);

# whole logs, stripped or unstripped
var wood_whole = <ore:logWoodWhole>;
for item in wood_raw.items {
  wood_whole.add(item);
}
for item in <ore:logStripped>.items {
  wood_whole.add(item);
}

# campfire-ready split logs (thanks TAN)
var log_wood = <ore:logWood>;
var log_campfire = <ore:logCampfire>;
log_campfire.mirror(log_wood);

for item in log_wood.items {
  log_wood.remove(item);
}
log_wood.add(<primal:logs_split_oak>);
log_wood.add(<primal:logs_split_spruce>);
log_wood.add(<primal:logs_split_birch>);
log_wood.add(<primal:logs_split_jungle>);
log_wood.add(<primal:logs_split_acacia>);
log_wood.add(<primal:logs_split_bigoak>);
log_wood.add(<primal:logs_split_yew>);

var tool_shovel = <ore:toolShovel>;

tool_shovel.add(<primal:flint_shovel>);
