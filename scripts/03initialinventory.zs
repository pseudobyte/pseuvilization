import crafttweaker.item.IItemStack;

var survival_manual = <patchouli:guide_book>.withTag({"patchouli:book": "patchouli:survival"}) as IItemStack;
mods.initialinventory.InvHandler.addStartingItem(survival_manual);

events.onPlayerRespawn(function(event as crafttweaker.event.PlayerRespawnEvent) {
  if (event.endConquered) { return; }
  var player = event.player;
  var letter = <forestry:letters:1>.withTag({PRC: 1 as byte, SDR: {profile: {Name: "Mr. Survival"}, TP: "player"}, INV: [{Slot: 0 as byte, id: "patchouli:guide_book", Count: 1 as byte, tag: {"patchouli:book": "patchouli:survival"}, Damage: 0 as short}], RC: {profile: {Name: event.player.name}, TP: "player"}, TXT: "Dear "+event.player.name+", since you enjoy dying so much, I thought you might like this book. Yours, Mr. Survival", UID: ""});
  player.give(letter);
});
